import numpy as np
from mean_estimator import MeanEstimator
from abstract_estimators import AbstractEstimators

# pb de première ligne --> demander au prof
class Variance(AbstractEstimators):
  
    def fit(self,table):
        var = []
        moy = MeanEstimator()
        moyenne_colonne = moy.fit(table)
        nouveau_table = np.array(table)
        nb_colonne = nouveau_table.shape[1]
        nb_ligne = nouveau_table.shape[0]
        difference_total = 0
        for i in range(nb_colonne):
            difference = np.sum((nouveau_table[:,i] - moyenne_colonne[i])**2)/nb_ligne
            var.append(difference)
                
        return var

if __name__ == "__main__":
    tab = [[1,7,3],[4,5,6]]
    test = VarianceEstimator()
    print(test.fit(tab))