import numpy as np
import matplotlib.pyplot as plt
import os

class Kmeans:
    def __init__(self,K, IterationMax=20):
        self.K=K
        self.N=0
        self.D=0
        self.IterationMax = IterationMax
        self.affectations = np.zeros((self.N))
        self.representants = np.zeros((self.K,self.D))

    def fit(self,X):
        self.N = X.shape[0]
        self.D = X.shape[1]
        representants_initiaux = np.random.random((self.K,self.D))
        representants = representants_initiaux       
        for iterations in range(self.IterationMax):
            affectations = self.maj_affectations(X, representants)
            representants = self.maj_representants(X, affectations)
        self.representants = representants
        self.affectations = affectations
        return self.representants
        
    def dist(self,x1,x2):
        dist = 0.0
        dimen = len(x1)
        for i in range(dimen):
            dist += ((x1[i]-x2[i])**2)
            return dist**0.5
        
    def maj_affectations(self, X, r):
        a = np.zeros((self.N))
        for n in range(self.N):
            distances = np.zeros(self.K)
            for k in range(self.K):
                distances[k] = self.dist(X[n], r[k])
            a[n] = np.argmin(distances, axis=0)
        return a

    def barycentre(self,X):
        Nselection = X.shape[0]
        if Nselection != 0:
            resultat = X.sum(axis=0)/Nselection
        else:
            resultat = X.sum(axis=0)*0.0
        return resultat
    
    def maj_representants(self, X, a):   
        d = X.shape[1]
        K = self.K
        r = np.zeros((K,d))
        for k in range(self.K):
            masque = (a == k) ## cette condition booléenne permet d'obtenir un masque 
            r[k] = self.barycentre(X[masque])
        return r

if __name__ == "__main__":
    # tab = [[1,2,3],[4,5,6]]
    
    #tab = np.loadtxt("tp1-data-2.dat")
   
    test = [[1,2,3],[4,5,6],[7,8,9]]
    tab = np.array(test)
    k = 3
    km = Kmeans(k,20)
    km.fit(tab)
    affectations = km.affectations
    rep =km.representants
    print(rep)
    print(affectations)
#     for i in range(k):
#         g0 = affectations==i
#         plt.scatter(tab[g0,0],tab[g0,1])



