import numpy as np
from abstract_estimators import AbstractEstimators
from commons_utils import CommonsUtils

class MeanEstiimator(AbstractEstimators):

    def fit(self,table):
        tmp = CommonsUtils()
        new_table = np.array(table)
        nb_colonne = new_table.shape[1]
        nb_ligne = new_table.shape[0]
        moy = []
        
        for i in range(nb_colonne):
            moyenne_colonne = tmp.means(new_table[:,i])
            moy.append(moyenne_colonne)
        return moy

if __name__ == "__main__":
    tab = [[10,8,30],[4,5,6]]
    test = MeanEstiimator()
    print(test.fit(tab))    