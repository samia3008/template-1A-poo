from abc import ABC, abstractmethod

class AbstractEstimators(ABC):

    @abstractmethod
    def fit(self, table): 
        pass
