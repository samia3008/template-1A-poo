import numpy as np
from abstract_estimators import AbstractEstimators


class Moy(AbstractEstimators):

    def fit(self,table):
        nouveau_table = np.array(table)
        nb_colonne = nouveau_table.shape[1]
        nb_ligne = nouveau_table.shape[0]
        moy = []
        for i in range(nb_colonne):
            somme_colonne = np.sum(nouveau_table[:,i])
            moyenne_colonne = somme_colonne/nb_ligne
            moy.append(moyenne_colonne)
        return moy


if __name__ == "__main__":
    tab = [[1,8,3],[4,5,6]]
    test = MeanEstimator()
    print(test.fit(tab))
            




