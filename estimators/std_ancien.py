import numpy as np
from variance_estimator import VarianceEstimator
from abstract_estimators import AbstractEstimators

class Std(AbstractEstimators):

    def fit(self,table):
        variance = VarianceEstimator()
        variance_colonne = variance.fit(table)
        ecarttype_colonne = np.sqrt(variance_colonne)
        return ecarttype_colonne

if __name__ == "__main__":
    tab = [[1,2,3],[4,5,6]]
    test = StdEstimator()
    print(test.fit(tab))