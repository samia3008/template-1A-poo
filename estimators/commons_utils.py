import numpy as np

class CommonsUtils():

    def means(self,liste):
        nb_element = len(liste)
        somme = 0
        moyenne = 0
        for i in range(nb_element):
            somme +=liste[i]
        moyenne = somme/nb_element
        return moyenne

    def  std(self,liste):
        nb_element = len(liste)
        outil = CommonsUtils()
        moy = outil.means(liste)
        difference = 0
        for i in range(nb_element):
            difference += ((liste[i]-moy)**2)
        var = difference/nb_element
        std = np.sqrt(var)
        return std


if __name__ == "__main__":
    tab = [1,2,3,4,5,6,7]
    test = CommonsUtils()
    print(test.means(tab))
    print(test.std(tab))
    tab2 = [10,2,3,4,2,9,5]
    print(test.means(tab2))
    print(test.std(tab2))