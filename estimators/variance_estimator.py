from commons_utils import CommonsUtils
from abstract_estimators import AbstractEstimators
import numpy as np

class VarianceEstimator(AbstractEstimators):
    def fit(self,table):
        var = []
        tmp = CommonsUtils()
        new_table = np.array(table)
        nb_colonne = new_table.shape[1]
        for i in range(nb_colonne):
            std_colonne = tmp.std(new_table[:,i])
            var_colonne = std_colonne**2
            var.append(var_colonne)
                
        return var
    
if __name__ == "__main__":
    tab = [[1,2,8],[4,5,6]]
    test = VarianceEstimator()
    print(test.fit(tab))     
