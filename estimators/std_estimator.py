import numpy as np
from abstract_estimators import AbstractEstimators
from commons_utils import CommonsUtils

class StdEstimator(AbstractEstimators):

    def fit(self,table):
        std = []
        tmp = CommonsUtils()
        new_table = np.array(table)
        nb_colonne = new_table.shape[1]
        for i in range(nb_colonne):
            std_colonne = tmp.std(new_table[:,i])
            std.append(std_colonne)
                
        return std
    
if __name__ == "__main__":
    tab = [[1,2,8],[4,5,6]]
    test = StdEstimator()
    print(test.fit(tab))     

      