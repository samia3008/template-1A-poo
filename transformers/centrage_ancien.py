############ pb ######################
import sys 
sys.path.append(r'estimators/')
from mean_estimator import MeanEstimator
from csv_importer import CsvImporter
import numpy as np

class Centrage():
    def __init__(self, table):
        self.table = table
    
    def execute(self):
        tab = []
        res = self.table
        nb_ligne = len(self.table)
        nb_colonne = len(self.table[0])
        
        for i in range(1, nb_ligne):
            tab.append((self.table[i])) # enlever la première ligne qui contient les noms des variables
        # print(len(tab))
        moyenne_colonne = MeanEstimator().fit(tab)
        # print(moyenne_colonne)

        for i in range(1,nb_ligne):
            for j in range(nb_colonne):
                res[i][j] = res[i][j] - moyenne_colonne[j]
        return res

if __name__ == '__main__':
    # test = Chargementcsv("data/", "age.csv")
    # table = test.execute()
    #print(table)
    #print(type(table[1][1]))
    #test2 = Centrage(table).execute()
    #print(test2.execute())

    test1 = [["a","a,","a"],[1,2,3],[2,3,4]]
    test2 = Centrage(test1).execute()
    print(test2)


