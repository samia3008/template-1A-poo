from abc import ABC, abstractmethod

class AbstractTransformers(ABC):

    @abstractmethod
    def transform(self,table):
        pass