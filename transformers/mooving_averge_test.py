import numpy as np
class Moyenne_glissante:

    def test_moyenne_glissante(self,valeurs, intervalle):
        indice_debut = (intervalle - 1)//2
        list_moyennes = []
        # for i in range(0,indice_debut):
        #     list_moyennes.append("neant")

        for i in range(indice_debut,len(valeurs)-indice_debut):
            sum = 0
            for j in range(i-indice_debut,i+indice_debut+1):
                sum += valeurs[j]   
            list_moyennes.append(sum/intervalle)

        # for i in range(0,indice_debut):
        #     list_moyennes.append("neant")
        return list_moyennes

if __name__ == '__main__':
    valeurs = [2,3,5,8,8,7,8,5,2,10]
    intervalle = 4
    test = Moyenne_glissante()
    print(test.test_moyenne_glissante(valeurs,4))

    valeurs2 = [1, 5, 8, 9, 4, 45, 85, 100, 85, 96]
    intervalle2 = 4
    test2 = Moyenne_glissante()
    print(test2.test_moyenne_glissante(valeurs2,intervalle2))
    # a = np.arange(20) 
    # moving_average(a) 
    # array([ 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15., 16., 17., 18.]) 
    # moving_average(a, n=4) 
    # array([ 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5, 14.5, 15.5, 16.5, 17.5])