import numpy as np
# from csv_importer import CsvImporter
from abstract_transformers import AbstractTransformers

class FilterVarsTransformers(AbstractTransformers):
    def __init__(self,vars,Type="keep"):
        self.__vars = vars
        self.__type = Type

    def transform(self,table):
        nouveau_table = np.array(table)
        nb_colonne = nouveau_table.shape[1]
        nb_ligne = nouveau_table.shape[0]
        table_sortie = []
        for i in range(nb_ligne) :
            a = []
            for j in range(nb_colonne):
                if nouveau_table[0,j] in self.__vars:
                    a.append(nouveau_table[i,j])
            table_sortie.append(a)
        return table_sortie
        

if __name__ == "__main__":
    # test = CsvImporter("data/age.csv")
    # table = test.Import()
    # test2 = Selection(table, ['departement','75 et plus','0-14'])
    # print(test2.execute())
    test = [['a','b','c'],[1,2,3],[4,5,6]]
    data = FilterVarsTransformers(['a','c'])
    print(data.transform(test))