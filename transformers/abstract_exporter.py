from abc import ABC, abstractmethod

class AbstractExporter(ABC):
    def __init__ (self,path,encoding):
        self.path = path
        self.encoding = encoding

    @abstractmethod
    def transform(self,table):
        pass



