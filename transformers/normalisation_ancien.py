import sys 
sys.path.append(r'estimators/')
from std_estimator import StdEstimator
from centrage import Centrage
import numpy as np

class Normalisation():
    def __init__(self, table):
        self.table = table
    
    def execute(self):
        tab = []
        nb_ligne = len(self.table)
        nb_colonne = len(self.table[0])
        tab_centree = Centrage(self.table).execute()
        res = tab_centree

        for i in range(1, nb_ligne):
            tab.append((tab_centree[i])) # récupérer la table centréé par le centrage
                                        # enlever la première ligne qui contient les noms des variables
        ecarttype_colonne = StdEstimator().fit(tab)

        for i in range(1,nb_ligne):
            for j in range(nb_colonne):
                res[i][j] = res[i][j] / ecarttype_colonne[j]
        return res

if __name__ == '__main__':

    test1 = [["a","a,","a"],[100,2,3],[2,9,4]]
    test2 = Normalisation(test1)
    print(test2.execute())