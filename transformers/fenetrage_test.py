from datetime import date as dt
from datetime import timedelta
from csv_importer import CsvImporter


class Fenetrage():
    def fenetrage(self, data, date_debut, date_fin):
        liste_date = []
        for i in range((dt.fromisoformat(date_fin) - dt.fromisoformat(date_debut)).days + 1):
            liste_date.append(str(dt.fromisoformat(date_debut) + timedelta(days=i)))
        new_data = []
        for date in liste_date:
            for index_date,header in enumerate(data[0]):
                if header == 'jour':
                    for liste in data:
                        if liste[index_date] == date:
                            new_data.append(liste)
        return new_data

if __name__ == '__main__':
    test = CsvImporter("data/donnees-hospitalieres-covid19-2021-03-03-17h03.csv")
    table = test.Import()
 #   print(table)
    fene = Fenetrage()
    resu = fene.fenetrage(table,'2020-03-18','2020-03-19')
    print(resu)
    print(len(resu))