import csv
from abstract_exporter import AbstractExporter


class CsvExporter(AbstractExporter):
    def __init__(self, path,encoding= 'ISO-8859-1'):
        super().__init__(path, encoding)
        
    def transform(self,table):
        with open(self.path,'w', encoding  = self.encoding, newline='') as csvfile :
            covidreader = csv.writer(csvfile, delimiter = ";")
            for row in table :
                covidreader.writerow(row)
    
    
if __name__ == "__main__":
    tab = [["a","b","c"],[1,2,3],[4,5,6]]
    data = CsvExporter("data/exporter_test.csv")
    data.transform(tab)
            