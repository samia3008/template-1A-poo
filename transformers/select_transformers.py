
from abstract_transformers import AbstractTransformers
from datetime import date as dt
from datetime import timedelta
from csv_importer import CsvImporter

class SelectTransformers(AbstractTransformers):
    def __init__(self,dateB,dateE):
        self.__dateBegin = dateB
        self.__dateFin = dateE

    def transform(self,table):
        liste_date = []
        for i in range((dt.fromisoformat(self.__dateFin) - dt.fromisoformat(self.__dateBegin)).days + 1):
            liste_date.append(str(dt.fromisoformat(self.__dateBegin) + timedelta(days=i)))
        new_table = []
        for date in liste_date:
            for index_date,header in enumerate(table[0]):
                if header == 'jour':
                    for liste in table:
                        if liste[index_date] == date:
                            new_table.append(liste)
        return new_table

if __name__ == '__main__':
    test = CsvImporter("data/donnees-hospitalieres-covid19-2021-03-03-17h03.csv")
    table = test.Import()
 #   print(table)
    fene = SelectTransformers('2020-03-18','2020-03-19')
    resu = fene.transform(table)
    print(resu)
    print(len(resu))  