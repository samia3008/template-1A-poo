import numpy as np
from csv_importer import CsvImporter

class Table():
    def __init__(self,table):
        self.header = table[0]
        table.pop(0)
        self.body = table
    
    def addLine(self, line, number):
        tmp = self.body
        tmp.insert(number-1, line)
        self.body = tmp

    def removeLine(self, number):
        tmp = self.body
        tmp.pop(number-1)
        self.body = tmp

    def addColumn(self, column, name,position):
        new_table = np.array(self.body)
        tmp = np.insert(new_table,position-1,values=column,axis=1)
        self.body = tmp.tolist()
        self.header.insert(position-1,name)
    
    def removeColumn(self,position):
        new_table = np.array(self.body)
        tmp = np.delete(new_table,position-1,axis=1)
        self.body = tmp.tolist()
        self.header.pop(position-1)

if __name__ == "__main__":
    test = [['a','b','c'],[1,2,3],[4,5,6]]
    data = Table(test)
    print(data.header)
    print(data.body)

    data.addLine([0,0,0],2)
    print(data.body)

    data.removeLine(2)
    print(data.body)

    data.addColumn([2,2],'d',4)
    print(data.body)
    print(data.header)

    data.removeColumn(4)
    print(data.body)
    print(data.header)