import csv
from abstract_importer import AbstractImporter


class CsvImporter(AbstractImporter):
    def __init__(self, file,encoding= 'ISO-8859-1'):
        super().__init__(file, encoding)
        
    def Import(self): # question : attribu table sert à quoi?
        table = []
        with open(self.file, encoding  = self.encoding) as csvfile :
            covidreader = csv.reader(csvfile, delimiter = ";")
            for row in covidreader :
                table.append((row))
        return table

if __name__ == "__main__":
    test = CsvImporter("data/covid-hospit-incid-reg-2021-03-03-17h20.csv")
    print(test.Import())

