import numpy as np
from abstract_transformers import AbstractTransformers
from csv_importer import CsvImporter

class MoovingAverageTransformers(AbstractTransformers):

    def __init__(self,periode):
        self.__periode = periode

    def transform(self,table):
        new_table = np.array(table)
        nb_colonne = new_table.shape[1]
        nb_ligne = new_table.shape[0]
        indice_debut = (self.__periode - 1)//2
        moy = []
        for c in range(nb_colonne):
            listes_moyennes = []
            colone = new_table[:,c]
            nbc = colone.shape[0]
            for i in range(indice_debut,nbc-indice_debut):
                sum = 0
                for j in range(i-indice_debut,i+indice_debut+1):
                    sum += colone[j]
                listes_moyennes.append(sum/self.__periode)
            moy.append(listes_moyennes)
        return moy
if __name__ == '__main__':
    table = [[2,1],[3,5],[5,8],[8,9],[8,4],[7,45],[8,85],[5,100],[2,85],[10,96]]
    moy_gli = MoovingAverageTransformers(4)
    resu = moy_gli.transform(table)
    print(resu)