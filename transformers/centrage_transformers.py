import sys 
sys.path.append(r'estimators/')
from abstract_transformers import AbstractTransformers
from csv_importer import CsvImporter
from commons_utils import CommonsUtils
import numpy as np

class CentrageTransformers(AbstractTransformers):
    
    def transform(self,table):
        res = table
        new_table = np.array(table)
        nb_colonne = new_table.shape[1]
        nb_ligne = new_table.shape[0]
        tmp = CommonsUtils()
        for i in range(nb_ligne):
            for j in range(nb_colonne):
                moy_colonne = tmp.means(new_table[:,j]) 
                res[i][j] -= moy_colonne
        return res

if __name__ == '__main__':

    tab = [[1,9,3],[2,3,5]]
    test2 = CentrageTransformers()
    print(test2.transform(tab))


