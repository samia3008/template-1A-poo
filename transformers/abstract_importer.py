from abc import ABC, abstractmethod

class AbstractImporter(ABC):
    def __init__(self,file,encoding):
        self.file = file
        self.encoding = encoding

    @abstractmethod
    def Import(self): # question : attribu table sert à quoi?
        pass

