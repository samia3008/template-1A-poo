import json
from abstract_importer import AbstractImporter

class JsonImporter(AbstractImporter):
    def __init__(self, file,encoding = 'ISO-8859-1'): # encoding inutile pour les fichers jason
        super().__init__(file, encoding)
    
    def Import(self): # question : attribu table sert à quoi?
        with open(self.file) as json_file:
            table = json.load(json_file)
        return table


if __name__ == "__main__":
    test = JsonImporter("data/VacancesScolaires.json")
    print(test.Import())


