import sys 
sys.path.append(r'estimators/')
from commons_utils import CommonsUtils
import numpy as np
from abstract_transformers import AbstractTransformers

class NormalisationTransformers(AbstractTransformers):

 def transform(self,table):
        res = table
        new_table = np.array(table)
        nb_colonne = new_table.shape[1]
        nb_ligne = new_table.shape[0]
        tmp = CommonsUtils()
        for i in range(nb_ligne):
            for j in range(nb_colonne):
                moy_colonne = tmp.means(new_table[:,j]) 
                # print(moy_colonne)
                std_colonne = tmp.std(new_table[:,j])
                # print(std_colonne)
                res[i][j] = ((res[i][j] - moy_colonne)/std_colonne)
        return res   

if __name__ == '__main__':

    tab = [[10,2,3],[2,3,13],[4,6,10],[5,7,3]]
    test2 = NormalisationTransformers()
    print(test2.transform(tab))

