import sys 

sys.path.append(r'transformers/')
from abstract_importer import AbstractImporter
from abstract_transformers import AbstractTransformers
from abstract_exporter import AbstractExporter
from filter_vars_transformers import FilterVarsTransformers
#from aggregation_transformers import AggregationTransformers
from centrage_transformers import CentrageTransformers
from csv_importer import CsvImporter
from csv_exporter import CsvExporter
from  filter_vars_transformers import FilterVarsTransformers
from json_importer import JsonImporter
# from map_exporter
from mooving_average_transformers import MoovingAverageTransformers
from normalisation_transformers import NormalisationTransformers
from select_transformers import SelectTransformers
from table import Table

sys.path.append(r'estimators/')
from abstract_estimators import AbstractEstimators
from kmeans import Kmeans
from mean_estimator import MeanEstiimator
from std_estimator import StdEstimator
from variance_estimator import VarianceEstimator

class Pipeline:
    def __init__(self,operations):
        self.operations = operations
    
    def run(self,table):
        tab = table
        for operation,position in self.operations:
            if isinstance(position,AbstractTransformers):
                tab = position.transform(tab)
                print("transformers done")
            elif isinstance(position, AbstractEstimators):
                tab = position.fit(tab)
                print("estimators done")
        
            # elif isinstance(poistion,Exporter):
            #     position.transform(tab)
        print('pipeline done')

if __name__ == "__main__":
    test = [[1,2,3],[4,5,6],[7,8,9]]
    pip = Pipeline( [ ("transformer data", FilterVarsTransformers([1,3])),
               ("Estimator data means", MeanEstiimator()) ] )
    pip.run(test)